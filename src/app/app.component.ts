import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { CameraPage } from '../pages/camera/camera';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = CameraPage;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {
   
    let config = {
      apiKey: "AIzaSyCaY-BY_AsIJYuXdlF8FfsYYU9RK_mippg",
      authDomain: "smarthair-175405.firebaseapp.com",
      databaseURL: "https://smarthair-175405.firebaseio.com",
      projectId: "smarthair-175405",
      storageBucket: "smarthair-175405.appspot.com",
      messagingSenderId: "600167383716"
    };
    firebase.initializeApp(config);
  

    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }
}

